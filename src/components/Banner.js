import React from "react";

import { Jumbotron, Button, Container, Col, Row } from "react-bootstrap";

export default function Banner() {
  return (
    //   React-Bootstrap Version
    <Container fluid>
      <Row>
        <Col className="px-0">
          <Jumbotron>
            <h1>Zuitt Coding Bootcamp</h1>
            <p>Oppurtunities for Everyone, Everywhere.</p>
            <p>
              <Button variant="primary">Enroll</Button>
            </p>
          </Jumbotron>
        </Col>
      </Row>
    </Container>
  );
}

// Bootstrap Version
/* (
  <div className="container-fluid">
    <div className="row justify-content-center">
      <div className="col-10 col-md-8 ">
        <div className="jumbotron">
          <h1>Zuitt Coding Bootcamp</h1>
          <p>Oppurtunities for Everyone, Everywhere</p>
          <button className="btn btn-primary">Enroll</button>
        </div>
      </div>
    </div>
  </div>
); */
