import React from "react";
import { Container } from "react-bootstrap";

// components
import Course from "./../components/Course";

// mock-data
import courses from "./../mock-data/courses";

export default function Courses() {
  let CourseCards = courses.map((course) => {
    return <Course key={course.id} course={course} />;
  });

  return <Container fluid>{CourseCards}</Container>;
}
