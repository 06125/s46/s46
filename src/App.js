import React, { Fragment } from "react";

import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
// import CourseCard from "./components/CourseCard";
// import Welcome from "./components/Welcome";

import Courses from "./pages/Courses";

export default function App() {
  return (
    <Fragment>
      <AppNavbar />
      <Home />
      {/* <CourseCard />
       */}
      <Courses />

      {/* <Welcome name="John Paul" />
    <Welcome name="Rayvin" /> */}
    </Fragment>
  );
}
